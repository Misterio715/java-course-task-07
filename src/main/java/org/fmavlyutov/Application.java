package org.fmavlyutov;

import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.model.Command;
import org.fmavlyutov.util.NumberUtil;

import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        displayHello();
        displayArguments(args);
        run();
    }

    private static void run() {
        String arg = "";
        Scanner scanner = new Scanner(System.in);
        while (!arg.equals(CommandLineConstant.EXIT)) {
            arg = scanner.next();
            displayCommands(arg);
        }
    }

    private static void displayArguments(String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        for (String arg : args) {
            switch (arg) {
                case CommandLineArgument.HELP:
                    displayHelp();
                    break;
                case CommandLineArgument.VERSION:
                    displayVersion();
                    break;
                case CommandLineArgument.ABOUT:
                    displayAbout();
                    break;
                case CommandLineArgument.INFO:
                    displayInfo();
                    break;
                default:
                    displayArgumentError();
            }
        }
    }


    private static void displayCommands(String arg) {
        if (arg == null || arg.isEmpty()) {
            return;
        }
        switch (arg) {
            case CommandLineConstant.HELP:
                displayHelp();
                break;
            case CommandLineConstant.VERSION:
                displayVersion();
                break;
            case CommandLineConstant.ABOUT:
                displayAbout();
                break;
            case CommandLineConstant.INFO:
                displayInfo();
                break;
            case CommandLineConstant.EXIT:
                exit();
                break;
            default:
                displayCommandError();
        }
    }

    private static void displayHello() {
        System.out.println("-----HELLO-----\n");
    }

    private static void displayGoodbye() {
        System.out.println("-----GOODBYE-----");
    }

    private static void displayHelp() {
        System.out.println(Command.HELP);
        System.out.println(Command.VERSION);
        System.out.println(Command.ABOUT);
        System.out.println(Command.INFO);
        System.out.println(Command.EXIT);
        System.out.println();
    }

    private static void displayVersion() {
        System.out.println("Version: 1.0.0\n");
    }

    private static void displayAbout() {
        System.out.println("Author: Philip Mavlyutov\n");
    }

    private static void displayInfo() {
        long availableProccers = Runtime.getRuntime().availableProcessors();
        System.out.printf("Available processors: %d cores\n", availableProccers);
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.printf("Total memory: %s\n", NumberUtil.formatBytes(totalMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.printf("Maximum memory: %s\n", maxMemoryFormat);
        long usedMemory = totalMemory - Runtime.getRuntime().freeMemory();
        System.out.printf("Used memory: %s\n", NumberUtil.formatBytes(usedMemory));
        System.out.println();
    }

    private static void exit() {
        displayGoodbye();
        System.exit(0);
    }

    private static void displayCommandError() {
        System.out.printf("Incorrect command. Enter [%s] to see all commands\n", CommandLineConstant.HELP);
        System.out.println();
    }

    private static void displayArgumentError() {
        System.out.printf("Incorrect argument. Enter [%s] to see all arguments\n", CommandLineArgument.HELP);
        System.out.println();
    }

}
